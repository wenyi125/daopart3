package DAOp3;

import static org.junit.Assert.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.Test;


public class DAOImp3Test {

	@Test
	public void inserttest() throws SQLException {
		DAOImp3 daoimp = new DAOImp3();
		ConnectManager con = new ConnectManager();
		int id = 8;
		daoimp.insert(con.getconnection(), id, "innisfree", " javelin throw ", "2016-10-10");
		Statement st = con.getconnection().createStatement();
		ResultSet rs2 = st.executeQuery("SELECT * FROM basic WHERE id=" + id);
		rs2.last();
		assertEquals(id,rs2.getInt(1));
	}
	
	public void checkidtest() throws SQLException{
		DAOImp3 daoimp = new DAOImp3();
		ConnectManager con = new ConnectManager();
		assertTrue(daoimp.checkid(con.getconnection(), "insert", 5));
	}
	
	public void querytest() throws SQLException{
		DAOImp3 daoimp = new DAOImp3();
		ConnectManager con = new ConnectManager();
		assertNotNull(daoimp.query(con.getconnection(), 5));
	}

}
