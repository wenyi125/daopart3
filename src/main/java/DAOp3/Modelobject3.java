package DAOp3;

public class Modelobject3 {
	private int id;
	private String name;
	private String hobby;
	private String birth;
	private String action;
	
	Modelobject3(String action,int id,String name,String hobby,String birth){
		this.id = id;
		this.name=name;
		this.hobby=hobby;
		this.birth=birth;
		this.action=action;
	}
	Modelobject3(String action,int id){
		this.id = id;
		this.action=action;
	}
	
	public String getaction(){
		return action;
	}
	
	public int getid(){
		return id;
	}
	
	public String getname(){
		return name;
	}
	
	public String gethobby(){
		return hobby;
	}
	
	public String getbirth(){
		return birth;
	}
	
	
	

}
