package DAOp3;

import java.util.ArrayList;

import com.mysql.jdbc.Connection;

public interface DAO3 {
	public void insert(Connection con,int id,String name,String hobby,String birth);
	public void delete(Connection con,int id);
	public void update(Connection con,int id,String name,String hobby,String birth);
	public ArrayList<String> query(Connection con,int id);
}
