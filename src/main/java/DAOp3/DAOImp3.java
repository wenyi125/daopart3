package DAOp3;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

public class DAOImp3 implements DAO3 {

	@Override
	public void insert(Connection con, int id, String name, String hobby, String birth) {

		String query = " insert into basic (id, name, hobby, birth)" + " values (?, ?, ?, ?)";

		try {
			PreparedStatement preparedStmt = (PreparedStatement) con.prepareStatement(query);
			preparedStmt.setInt(1, id);
			preparedStmt.setString(2, name);
			preparedStmt.setString(3, hobby);
			preparedStmt.setDate(4, Date.valueOf(birth));
			preparedStmt.executeUpdate();
			System.out.println("Success!");
		} catch (SQLException e) {
			e.getMessage();
			e.printStackTrace();
		}
	}

	@Override
	public void delete(Connection con, int id) {
		String query = " DELETE FROM basic WHERE id =?";
		try {
			PreparedStatement preparedStmt = (PreparedStatement) con.prepareStatement(query) ;
			preparedStmt.setInt(1, id);
			preparedStmt.executeUpdate();
			System.out.println("Success!");
		} catch (SQLException e) {
			e.getMessage();
			e.printStackTrace();
		}

	}

	@Override
	public void update(Connection con, int id, String name, String hobby, String birth) {
		String query = "UPDATE basic SET name = ?, hobby = ?, birth = ? WHERE id = ?";
		try {
			PreparedStatement preparedStmt = (PreparedStatement) con.prepareStatement(query);
			preparedStmt.setString(1,name);
			preparedStmt.setString(2,hobby);
			preparedStmt.setDate(3,Date.valueOf(birth));
			preparedStmt.setInt(4,id);
			preparedStmt.execute();
			System.out.println("Success!");
		} catch (SQLException e) {
			e.getMessage();
			e.printStackTrace();
		}
		

	}

	@Override
	public ArrayList<String> query(Connection con, int id) {
		ArrayList<String> result = null;
		Statement st;
		try {
			st = (Statement) con.createStatement();
			ResultSet rs = st.executeQuery("SELECT * FROM basic WHERE id = "+id);
			rs.last();
			result = new ArrayList<String>();
			result.add(rs.getString("id"));
			result.add(rs.getString("name"));
			result.add(rs.getString("hobby"));
			result.add(rs.getString("birth"));
			for (int i=0;i<result.size();i++){
				System.out.print(result.get(i)+" ");
			}
			System.out.println();
			
		} catch (SQLException e) {
			e.getMessage();
			e.printStackTrace();
		}
		
		return result;
	}
	
	public void showDB(Connection con){
		try {
			Statement st = (Statement) con.createStatement();
			ResultSet rs = st.executeQuery("SELECT * FROM basic");
			while(rs.next()){
				String result = rs.getString(1)+"\t"
						          +rs.getString(2)+"\t"+rs.getString(3)+"\t"
				                    +rs.getString(4);
				System.out.println(result);
			}
		} catch (SQLException e) {
			e.getMessage();
			e.printStackTrace();
		}
		
	}
	
	public Boolean checkid (Connection con,String action,int id ){
		boolean idrepeat = false;
		Statement st;
		try {
			st = (Statement) con.createStatement();
			ResultSet rs = st.executeQuery("SELECT * FROM basic");
			while(rs.next()){
				if(String.valueOf(id).equals(rs.getString("id")))
					idrepeat = true;
			}
		} catch (SQLException e) {
			e.getMessage();
			e.printStackTrace();
		}
		if(action.equals("insert") && idrepeat == true){
			System.out.println("The ID is repeat");
			return false;
		}
		if(!action.equals("insert") && idrepeat == false){
			System.out.println("The ID isn't exist");
			return false;
		}
		return true;
		
		
	}


}
