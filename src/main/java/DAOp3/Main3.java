package DAOp3;

import java.sql.SQLException;

public class Main3 {

	public static void main(String[] args) throws SQLException {
		
		DAOImp3 action = new DAOImp3();
		ConnectManager con = new ConnectManager();
		Modelobject3 modelobject = (args.length > 2)? 
				new Modelobject3(args[0], Integer.parseInt(args[1]), args[2], args[3], args[4])
				: new Modelobject3(args[0], Integer.parseInt(args[1]));
				
		if (!action.checkid(con.getconnection(), modelobject.getaction(), modelobject.getid()))
			System.exit(0);
		
		switch(args[0]){
		case "insert":
			action.insert(con.getconnection(), modelobject.getid(), modelobject.getname(), modelobject.gethobby(), modelobject.getbirth());
			break;
		case "delete":
			action.delete(con.getconnection(), modelobject.getid());
			break;
		case "update":
			action.update(con.getconnection(), modelobject.getid(), modelobject.getname(), modelobject.gethobby(), modelobject.getbirth());
			break;
		case "query":
			action.query(con.getconnection(), modelobject.getid());
			break;
		default:
			System.out.println("Wrong argument");
			System.exit(0);
		}
		action.showDB(con.getconnection());
		con.connectOFF(con.getconnection());
	}

}
