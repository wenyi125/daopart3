package DAOp3;

import java.sql.DriverManager;
import java.sql.SQLException;

import com.mysql.jdbc.Connection;

public class ConnectManager {
	final static String DRIVER = "com.mysql.jdbc.Driver";
	final static String URL = "jdbc:mysql://localhost/Database_personal";
	final static String USER="root";
	final static String PASSWORD="123456";
	
	public Connection getconnection() throws SQLException{
		try {
			Class.forName(DRIVER);
		} catch (ClassNotFoundException e) {
			System.out.println("Can't connect to MySQL" + e.getMessage());
			e.printStackTrace();
			System.exit(0);
		}
		Connection con = (Connection) DriverManager.getConnection(URL, USER, PASSWORD);
	    
		return con;
		
		
	}
	
	public void connectOFF(Connection con){
		try {
			con.close();
			if (con.isClosed())
				System.out.println("The database is closed");
		} catch (SQLException e) {
			e.getMessage();
			e.printStackTrace();
		}
	}	   

}
